"use strict";
// Упражнение 1
let a='$100';
let b='300$';
a = a.replace(/[^0-9]/g, '');
b = b.replace(/[^0-9]/g, '');
let summ= +a + +b;// Ваше решение
console.log(summ);// Должно быть 400

// Упражнение 2
let message=' привет, медвед      ';
alert(message.trim() );
message= message[1].toUpperCase() + message.slice(2);// Решение должно быть написано тут
console.log(message);// “Привет, медвед”

// Упражнение 3
let msg;
let age=prompt('Сколько вам лет?');
if(age >= 0 && age <= 3) {
    msg=`Вам ${age} лет и вы младенец`;
}
if(age >= 4 && age <= 11) {
    msg=`Вам ${age} лет и вы ребенок`;
}
if(age >= 12 && age <= 18) {
    msg=`Вам ${age} лет и вы подросток`;
}
if(age >= 19 && age <= 40) {
    msg=`Вам ${age} лет и вы познаете жизнь`;
}
if(age >= 41 && age <= 80) {
    msg=`Вам ${age} лет и вы познали жизнь`;
}
if(age > 81) {
    msg=`Вам ${age} лет и вы долгожитель`;
}

alert(msg);