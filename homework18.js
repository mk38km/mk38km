// Упражнение 1
let a='100px';
let b='323px';
let result=parseInt(a) + parseInt(b);// Решение должно быть написано тут
console.log(result);// Выводим в консоль, должно получиться 423

// Упражнение 2
alert( Math.max (10, -45, 102, 36, 12, 0, -1) );

// Упражнение 3
let c=123.3399;// Округлить до 123
alert(Math.floor);// Решение
console.log(Math.round(c));

let d=0.111;// Округлить до 1
alert(Math.ceil);// Решение 0 --------- !???
console.log(Math.round(d));

let e=45.333333;// Округлить до 45.3
alert(e.toFixed(1));// Решение
console.log(Math.round(e));

let f=3;// Возвести в степень 5 (должно быть 243)
alert( 3 ** 5);// Решение
console.log(Math.round(f));

let g=400000000000000;// Записать в сокращенном виде
alert(4e14);// Решение
console.log(Math.round(g));

let h='1'!=1;// Поправить условие, чтобы результат был true (значения изменять нельзя, только оператор)
alert(h='1' ==1);
console.log(Math.round(h));